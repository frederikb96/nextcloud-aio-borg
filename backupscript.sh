#!/bin/bash

# Code taken from Nextcloud AIO and modified for this use case

# Functions
get_start_time(){
    START_TIME=$(date +%s)
    CURRENT_DATE=$(date --date @"$START_TIME" +"%Y%m%d_%H%M%S")
}
get_expiration_time() {
    END_TIME=$(date +%s)
    END_DATE_READABLE=$(date --date @"$END_TIME" +"%d.%m.%Y - %H:%M:%S")
    DURATION=$((END_TIME-START_TIME))
    DURATION_SEC=$((DURATION % 60))
    DURATION_MIN=$(((DURATION / 60) % 60))
    DURATION_HOUR=$((DURATION / 3600))
    DURATION_READABLE=$(printf "%02d hours %02d minutes %02d seconds" $DURATION_HOUR $DURATION_MIN $DURATION_SEC)
}

# Check if target is mountpoint
if ! mountpoint -q /mnt/borgbackup; then
    echo "/mnt/borgbackup is not a mountpoint which is not allowed"
    exit 1
fi

# Do not continue if this file exists (needed for simple external blocking)
if [ -f "$BORG_BACKUP_DIRECTORY/aio-lockfile" ]; then
    echo "Not continuing because aio-lockfile exists - it seems like a script is externally running which is locking the backup archive."
    echo "If this should not be the case, you can fix this by deleting the 'aio-lockfile' file from the backup archive directory."
    exit 1
fi

touch "$BORG_BACKUP_DIRECTORY/aio-lockfile"

# Do the backup


# If target empty
if ! [ -f "$BORG_BACKUP_DIRECTORY/config" ]; then
    echo "No borg config file was found in the targeted directory."
    exit 1
fi

# Perform backup
echo "Performing backup..."

# Borg options
# auto,zstd compression seems to has the best ratio based on:
# https://forum.level1techs.com/t/optimal-compression-for-borg-backups/145870/6
BORG_OPTS=(-v --stats --compression "auto,zstd" --exclude-caches)

# Create the backup
echo "Starting the backup..."
get_start_time
if ! borg create "${BORG_OPTS[@]}" "$BORG_BACKUP_DIRECTORY::$CURRENT_DATE-nextcloud-aio-manual" "/nextcloud_aio_volumes/"; then
    echo "Backup failed!"
    exit 1
fi

# Prune options
read -ra BORG_PRUNE_OPTS <<< "$BORG_RETENTION_POLICY"
echo "BORG_PRUNE_OPTS are ${BORG_PRUNE_OPTS[*]}"

# Prune archives
echo "Pruning the archives..."
if ! borg prune --stats --glob-archives '*_*-nextcloud-aio' "${BORG_PRUNE_OPTS[@]}" "$BORG_BACKUP_DIRECTORY"; then
    echo "Failed to prune archives!"
    exit 1
fi

# Compact archives
echo "Compacting the archives..."
if ! borg compact "$BORG_BACKUP_DIRECTORY"; then
    echo "Failed to compact archives!"
    exit 1
fi

# Inform user
get_expiration_time
echo "Backup finished successfully on $END_DATE_READABLE ($DURATION_READABLE)"
exit 0


