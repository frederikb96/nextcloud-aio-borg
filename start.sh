#!/bin/bash

# Code taken from Nextcloud AIO and modified for this use case

# Variables
export MOUNT_DIR="/mnt/borgbackup"
export BORG_BACKUP_DIRECTORY="$MOUNT_DIR/borg"

# Validate BORG_PASSWORD
if [ -z "$BORG_PASSWORD" ] && [ -z "$BACKUP_RESTORE_PASSWORD" ]; then
    echo "Neither BORG_PASSWORD nor BACKUP_RESTORE_PASSWORD are set."
    exit 1
fi

# Export defaults
if [ -n "$BACKUP_RESTORE_PASSWORD" ]; then
    export BORG_PASSPHRASE="$BACKUP_RESTORE_PASSWORD"
else
    export BORG_PASSPHRASE="$BORG_PASSWORD"
fi
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes
export BORG_RELOCATED_REPO_ACCESS_IS_OK=yes

# Validate BORG_MODE
if [ "$BORG_MODE" != backup ]; then
    echo "No correct BORG_MODE mode applied. Valid are 'backup'."
    exit 1
fi

export BORG_MODE

# Run the backup script
if ! bash /backupscript.sh; then
    FAILED=1
fi

# Remove aio lockfile
rm -f "$BORG_BACKUP_DIRECTORY/aio-lockfile"

if [ -n "$FAILED" ]; then
    echo "Overall failed!"
    exit 1
fi

exec "$@"
